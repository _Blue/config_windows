@echo off

REM Check for admin permissions
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"


if '%errorlevel%' NEQ '0' (
    echo Requesting administrative privileges...
    goto UACPrompt

) else ( goto gotAdmin )

:UACPrompt
    echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
    set params = %*:"=""
    echo UAC.ShellExecute "cmd.exe", "/c %~s0 %params%", "", "runas", 1 >> "%temp%\getadmin.vbs"
    "%temp%\getadmin.vbs"
    del "%temp%\getadmin.vbs"
    exit /B

:gotAdmin
    pushd "%CD%"
    CD /D "%~dp0"

where choco
if %ERRORLEVEL% EQU 0 goto :clone


rem Install chocolatey
@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin" || goto :error

:clone
rem Install git
choco install -y  git --params=/NoShellIntegration || goto :error

call refreshenv

rem Clone repo
md %userprofile%\Desktop\repos

git clone https://bitbucket.org/_Blue/config_windows %userprofile%\Desktop\repos\config_windows

cd %userprofile%\Desktop\repos\config_windows && cmd /k setup.bat


echo Done !
exit /B


:error
echo Something went wrong
pause
exit /B
