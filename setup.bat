@echo off

rem Check for admin permissions
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"


if '%errorlevel%' NEQ '0' (
    echo Requesting administrative privileges...
    goto UACPrompt

) else ( goto gotAdmin )

:UACPrompt
    echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
    set params = %*:"=""
    echo UAC.ShellExecute "cmd.exe", "/c %~s0 %params%", "", "runas", 1 >> "%temp%\getadmin.vbs"
    "%temp%\getadmin.vbs"
    del "%temp%\getadmin.vbs"
    exit /B

:gotAdmin
    pushd "%CD%"
    CD /D "%~dp0"


where choco
if %ERRORLEVEL% EQU 0 goto :setupreg


rem Install chocolatey
"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin" || goto :error

:setupreg
rem Install reg files
for /f %%e in ('dir /b reg') do echo merge: %%e && regedit.exe /S reg\%%e || goto :error

rem Setup appdata config files
xcopy AppData %userprofile%\appdata /e /y

rem Enable windows features
DISM /Online /NoRestart /Add-Capability /CapabilityName:Tools.DeveloperMode.Core~~~~0.0.1.0
DISM /online /NoRestart /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux
DISM /online /NoRestart /enable-feature /featurename:TelnetClient

rem Install packages
set packages=git notepadplusplus processhacker firefox cmder grep curl bind-toolsonly sed autohotkey

for %%e in (%packages%) do (
  choco install -y %%e || goto :error
)

rem Setup other config files
xcopy SystemDrive %homedrive%\  /e /y

rem Enable legacy boot
bcdedit /set bootmenupolicy Legacy

rem Remove windows store pre-installed apps (it seems not to do anything unless executed twice...)
rem powershell -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "Get-AppxPackage | where {$_.SignatureKind -eq \"Store\"} | where {-Not $_.Name.StartsWith(\"Microsoft\")}| Remove-AppxPackage"

rem Disable useless stasks
powershell -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "Get-Scheduledtask \"SmartScreenSpecific\",\"Microsoft Compatibility Appraiser\",\"Consolidator\",\"KernelCeipTask\",\"UsbCeip\",\"Microsoft-Windows-DiskDiagnosticDataCollector\", \"GatherNetworkInfo\",\"QueueReporting\" | Disable-scheduledtask"

rem Disable hibernate mode
powercfg -h off

rem Set high performance power plan
powercfg -setactive 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c

rem Disable sleep on lid close
for /f "tokens=* USEBACKQ" %%a in (`powercfg /getactivescheme`) do @set cfg=%%a

::trim power config output to get GUID
set trimcfg=%cfg:~19,36%

::make power scheme change (clid close action, sleep timeout, screen timeout)
powercfg /setdcvalueindex %trimcfg% 4f971e89-eebd-4455-a8de-9e59040e7347 5ca83367-6e45-459f-a27b-476b1d01c936 000
powercfg /setacvalueindex %trimcfg% 4f971e89-eebd-4455-a8de-9e59040e7347 5ca83367-6e45-459f-a27b-476b1d01c936 000
powercfg /setacvalueindex %trimcfg% 238c9fa8-0aad-41ed-83f4-97be242c8f20 29f6c1db-86da-48c5-9fdb-f2b67b1f44da 0
powercfg /setacvalueindex %trimcfg% 7516b95f-f776-4464-8c53-06167f40cc99 3c0bc021-c8a8-4e07-a973-6b14cbcb2b7e 0

::apply changes
powercfg /s %trimcfg%

rundll32.exe user32.dll,UpdatePerUserSystemParameters 1, True
%windir%\syswow64\rundll32.exe user32.dll,UpdatePerUserSystemParameters 1, True

::Disable auditing
Auditpol /set /category:"*" /Success:disable /failure:disable

echo All done
pause
exit /B

:error
echo Something went wrong
pause
exit /B

