# Blue's configuration for Windows

This repository contains configuration files for Windows, mainly as registry keys files (.reg).

## Setup

```
$ setup.bat
```

If needed, the script will ask for administrator permissions.

Here's a one-liner to clone this repository in `~/repos` and run install script

```
powershell -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "(new-object System.Net.WebClient).DownloadFile('http://windows.bluecode.fr/setup.bat','%TEMP%\bs-setup.bat'); %temp%/bs-setup.bat" 
``` 

Note that this one-liner is also available on `windows.bluecode.fr`.

## Requirements

Windows 10 >= build 1709

## Content

This script will install the following programs via chocolatey:

* notepad++
* ProcessHacker
* git
