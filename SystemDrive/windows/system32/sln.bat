@echo off

rem Let's keep it simple and assume there'll be only one sln file in the current directory

for %%f in (*.sln) do (

	if exist %%f (
    echo Found: %%f
	start "" "%%f"
	exit /b
	)
)

echo No solution file found
exit /b 1

