@echo off
if exist "%ProgramFiles(x86)%\Notepad++\notepad++.exe" (
	start  "" "%ProgramFiles(x86)%\Notepad++\notepad++.exe" %*
	exit /B
) else (
	 start "" "%ProgramFiles%\Notepad++\notepad++.exe" %*
)

exit /b