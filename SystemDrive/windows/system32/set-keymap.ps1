$ErrorActionPreference = "Stop"


$l = Get-WinUserLanguageList
$keymap=$l[0].InputMethodTips[0].split(':')[0] + ":0000040C"

$l[0].InputMethodTips.clear()
$l[0].InputMethodTips.add($keymap)
$l[0].InputMethodTips.add($keymap.split(':')[0] + ':00000409') 
Set-WinUserLanguageList -Force -LanguageList $l


$l[0].InputMethodTips.Clear()
$l[0].InputMethodTips.add($keymap)
Set-WinUserLanguageList -Force -LanguageList $l